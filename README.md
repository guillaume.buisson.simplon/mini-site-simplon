# Contenue

**Mini site pour Simplon.co**

![Texte Alt](https://simplon.co/images/logo-simplon.png)
    
## objectif

Créer un mini site pour **Simplon.co** dans le cadre de ma formation **Developpeur Web**
    
### languages

* HTML
* CSS
        
### Consignes
    
* Le site doit être responsive
* Vous devez utiliser SCSS
* Doit contenir une convention de nommage
* Doit contenir un formulaire (nom, prenom, email, genre, message)
* images optimisées
* SEO

    
## Groupe

* Olga Padkovenka
* Guillaume Buisson

# Charte graphique

## Couleurs: 

    Rouge (Simplon, réseau de Fabriques): #ce0033
	Rouge (Description): #d9002e
	Rouge (Notre impact): #ce0033
	Rouge (hover) : #a4082f
	Bleu/Gris: #1b3549
	Girs (Logo reseau) :#c9d8e4
	
## Font:

    font-family: Nunito Sans,sans-serif;
	font-size: 14px;
	font-weight: 700;
	color: #000;